[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# PRIMERA PRACTICA

## PROBLEMA A RESOLVER
### RESOLVER CON SEMÁFOROS

#### Buffer Selectivo

Se tiene un sistema con un número indeterminado de procesos productores y consumidores donde la operación de consumir es más lenta que la de producir, y se trata de que los consumidores no retarden la ejecución de los productores. Además, se tienen algunas características particulares:

-   Hay dos clases de datos, que llamaremos **A** y **B**.
    
-   Hay 2 clases de productores, los que solo producen datos de clase **A** y los que solo producen datos de clase **B**.
    
-   Hay 3 tipos de consumidores:
	-   Los que solo consumen datos de clase **A**.
    
	-   Los que solo consumen datos de clase **B**.
    
	-   Los que consumen datos de clase **A** o **B** indistintamente.
    
Los productores generan datos en rachas de 1 a 3, que deben ser almacenados "*de golpe*". Se tiene capacidad para almacenar **NA**  y **NB**  datos de clase **A** o **B**, respectivamente.

La lectura de los datos por parte de los consumidores debe hacerse respetando el orden en que han sido depositados por los productores.

#### Análisis

Para la resolución del problema necesitamos diferenciar los diferentes tipos de procesos y elementos que se almacenarán en el *Buffer* compartido, al tratarse de un problema **Productor/Consumidor** modificado, y que se utilizará en el diseño para minimizar el desarrollo de código necesario en la solución. En este caso será necesario definir un enumerado donde se incluyen los tipos necesarios:

```
TDA Enum TipoElemento { A, B, AoB }
```

De esta forma se pueden recoger las diferentes posibilidades que se requieren para los elementos a almacenar, los productores y los consumidores.

Para representar el elemento que se almacena en el *Buffer Selectivo* será necesario la definición para ese tipo de elemento de la siguiente forma:

```
TDA Elemento
	Variables
		TipoElemento tipoElemento
		Boolean finRacha
	Funciones:
		TipoElemento getTipoElemento()
		Boolean finRacha()
```

Con la definición de `Elemento` podemos representar los diferentes tipos que se almacenarán y poder averiguar la longitud de la *racha* que hay almacenada.

Como último elemento de información necesario es definir el *Buffer Selectivo* de la siguiente forma:

```
TDA BufferSelectivo
	Variables:
		Elemento elm
	Funciones:
		void add(Elemento) : Añade al final del Buffer
		Elemento get() : Devuelve y elimina el primer elemento del Buffer
		Elemento get(TipoElemento) : Devuelve el primer Elemento que coincide con el TipoElemento
```

Además serán necesario definir una serie de constantes que ayuda a la legibilidad del diseño de la solución:

```
MAX[TipoElemento] : Número máximo de elementos que pueden almacenar para cada tipo
MIN_RACHA : Mínimo número de elementos que forman una racha de producción/consumición
MAX_RACHA : Máximo número de elementos que forman una racha de producción/consumición
```

Si nos fijamos en la solución clásica del **Productor/Consumidor** son necesarios unos semáforos que nos permiten garantizar el acceso en exclusión mutua del *Buffer* y para la sincronización de los diferentes procesos implicados. Pero en nuestro caso necesitamos además tener contadores para los procesos implicados porque el número de elementos que se almacenan y consumen no es uno si no una racha variable. 

Si no podemos conocer el número de elementos almacenados en el buffer de cada tipo los productores tendrían que hacer una reserva previa en un semáforo que indicara los espacios disponibles en el *Buffer*. Pero este método podría ocasionar un **bloqueo** al no poder predecir las diferentes reservas individuales que tendrían que hacer cada uno de los productores. Es decir, para evitar este **bloqueo** es necesario garantizar que hay espacio suficiente en el *Buffer* de una sola vez. Por este motivo los procesos deben conocer los elementos de cada tipo que están almacenados en el *Buffer* en cada momento:

```
int elementos[TipoElemento] 
```

Estos contadores serán compartidos entre los procesos y deberán protegerse mediante un semáforo que se presentará más adelante. Además debemos tener un contador para llevar los productores que se han bloqueado por no poder almacenar su racha.

```
int numProductores[TipoElemento][racha]
```

Para los consumidores necesitamos el número de cada tipo que está esperando a consumir una racha almacenada en el *Buffer*. Esto es necesario porque hay un proceso consumidor que puede consumir cualquier elemento que está almacenado y deben sincronizarse correctamente cada proceso consumidor y de esta forma poder garantizar que hay elementos disponibles en el *Buffer*. Para ello se utilizará la siguiente variable:

```
int numConsumidores[TipoElemento]
```

Como en el caso anterior esta variable necesita estar protegida por un semáforo que garantice su acceso en exclusión mutua que presentaremos mas adelante.

##### Semáforos

Los semáforos necesarios, y su descripción, para la solución del problema serán los siguientes. Entre paréntesis se indica el valor de inicialización:

 - **Semáforos de exclusión mutua**
```
semaforo exmBuffer(1) : garantiza el acceso al Buffer para el productor o el consumidor
semaforo exmElm(1) : garantiza el acceso a los contadores de cada tipo de elemento almacenado en el Buffer
semaforo exmConsumidores(1) : garantiza el acceso a los contadores de los consumidores bloqueados
```

- **Semáforos de sincronización**
```
semaforo sincProductor[TipoElemento][racha](0) : bloquea a los productores de un tipo y tamaño de racha 
                                                 hasta que haya espacio en el Buffer
semaforo sincConsumidor[TipoElemento](0) : bloquea a los consumidores de un tipo hasta que haya 
                                           un elemento disponible para ellos en el Buffer 
```

#### Diseño

El pseudocódigo para los diferentes procesos es el siguiente:

Para los procesos consumidores:

```
Proceso Productor(tipoElemento)
	tipoElemento // Tipo de elemento a producir {A, B}
	
Variables
	int racha // Tamaño de la racha a producir

ejecucion_proceso { // Hilo de ejecución del proceso
	genera(racha) // Genera un valor entre [MIN_RACHA,MAX_RACHA]
	
	// Nos aseguramos que podemos almacenar la racha
	// o se bloquea el productor en el tamaño de la racha
	exmElm.wait()
	if( elementos[tipoElemento] + racha <= MAX[tipoElemento] )
		elmementos[tipoElemento] += racha
		exmElm.signal()
	else
		numProductores[tipoElemento][racha]++
		exmElm.signal()
		sincProductor[tipoElemento][racha].wait()
	
	// Se almacena la racha
	exmBuffer.wait()
	for i = 1 until racha
		if i = racha
			finRacha = true
		else
			finRacha = false
		buffer.add(elemento(tipoElemento,finRacha))
	exmBuffer.signal()

	// Liberamos a un consumidor
	exmConsumidores.wait()
	if( numConsumidores[AoB] > 0 )
		numConsumidores[AoB]--
		sincConsumidor[AoB].signal()
	else //Modificado el 23/03/2021
		numConsumidores[tipoElemento]--
		sincConsumidor[tipoElemento].signal()
	exmConsumidores.signal()

	// Comprobamos si tenemos que liberar un productor
	// porque hay sitio en el buffer para producir
	exmElm.wait()
	liberado = false
	tamañoRacha = MAX_RACHA
	while( tamañoRacha >= MIN_RACHA & !liberado )
		if( elementos[tipoElemento] + tamañoRacha <= MAX[tipoElemento] &&
		    numProductores[tipoElemento][tamañoRacha] > 0) 
			elementos[tipoElemento] += tamañoRacha
			numProductores[tipoElemento][tamañoRacha]--
			liberado = true
			sincProductor[tipoElemento][tamañoRacha].signal()
		tamañoRacha--
	exmElm.signal()
}
```

Para los procesos consumidores:

```
Proceso Consumidor(tipoElemento)
	tipoElemento // Tipo de elemento a consumir {A, B, AoB}
	
Variables
	int racha // Tamaño de la racha consumida

ejecucion_proceso { // Hilo de ejecución del proceso
	// Sumamos el consumidor para saber los que están
	// esperamos hasta que haya un elemento de su tipo
	exmConsumidores.wait()
	numConsumidores[tipoElemento]++
	exmConsumidores.signal()
	sincConsumidores[tipoElemento].wait()
	
	// Consumimos la racha del tipo apropiado
	exmBuffer.wait()
	racha = 0
	repeat
		if( tipoElemento == AoB )
			elemento = buffer.get()
		else 
			elemento = buffer.get(tipoElemento)
		racha++
	until elemento.finRacha()
	exmBuffer.singal()

	// Liberamos a un productor del tipo apropiado
	exmElm.wait()
	tipo = elemento.getTipoElemento()
	elementos[tipo] -= racha
	liberado = false
	tamañoRacha = MAX_RACHA
	while( tamañoRacha >= MIN_RACHA & !liberado )
		if( elementos[tipo] + tamañoRacha <= MAX[tipo] &&
		    numProductores[tipo][tamañoRacha] > 0) 
			numProductores[tipo][tamañoRacha]-- 
			elementos[tipo] += tamañoRacha
			liberado = true
			sincProductor[tipo][tamañoRacha].signal()
		tamañoRacha--
	exmElm.signal()
}
```
