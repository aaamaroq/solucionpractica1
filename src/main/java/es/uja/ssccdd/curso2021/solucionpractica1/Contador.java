/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.solucionpractica1;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Contador {
    private int contador;

    public Contador(int contador) {
        this.contador = contador;
    }

    public int getContador() {
        return contador;
    }

    public int añadir(){
    
        return ++contador;
        
    }
    
    public int restar(){
    
        return --contador;
        
    }
    
    
}
