/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.solucionpractica1;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Contantes {
    // Generador aleatorio
    public static final Random aleatorio = new Random();
    
    // Tipos de elementos para el problema
    public enum TipoElemento {A, B, AoB}
    public static int TIPOS_CONSUMIDORES = 3;
    public static int TIPOS_PRODUCTORES = 2;
    
    // Constantes
    public static final int TOTAL_PRODUCTORES = 10;
    public static final int TOTAL_CONSUMIDORES = 10;
    public static final int PRIMERO = 0;
    public static final int NO_ENCONTRADO = -1;
    public static final int MIN_RACHA = 1;
    public static final int MAX_RACHA = 3;
    public static final int[] MAX_ELEMENTOS = {10,10}; // Elementos máximo para cada tipo
    public static final int TIEMPO_PRODUCCION = 2;
    public static final int TIEMPO_CONSUMICION = 5;
    public static final int TIEMPO_ESPERA = 1;
}
