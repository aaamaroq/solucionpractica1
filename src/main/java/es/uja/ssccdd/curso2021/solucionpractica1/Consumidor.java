/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.solucionpractica1;

import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.MAX_RACHA;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.MIN_RACHA;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TIEMPO_CONSUMICION;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.PRIMERO;
import es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TipoElemento;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TipoElemento.AoB;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.MAX_ELEMENTOS;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Consumidor implements Runnable {

    private final String iD;
    private final Contantes.TipoElemento tipoElemento;
    private final int[] elementos;
    private final Buffer buffer;
    private final Semaphore exmElm;
    private final Semaphore exmBuffer;
    private final Semaphore exmSelectorAB;
    private final Semaphore[][] sincProductor;
    private final Semaphore[] sincConsumidor;
    private final ArrayList<TipoElemento> selectorAB;
    private final Contador contadorAB;

    // Tipo elemento consumido
    private TipoElemento tipoConsumido;

    public Consumidor(String iD, Contantes.TipoElemento tipoElemento, int[] elementos, Buffer buffer,
            Semaphore exmElm, Semaphore exmBuffer, Semaphore exmSelectorAB, Semaphore[][] sincProductor,
            Semaphore[] sincConsumidor, ArrayList<TipoElemento> selectorAB, Contador contadorAB) {
        this.iD = iD;
        this.tipoElemento = tipoElemento;
        this.elementos = elementos;
        this.buffer = buffer;
        this.exmElm = exmElm;
        this.exmBuffer = exmBuffer;
        this.exmSelectorAB = exmSelectorAB;
        this.sincProductor = sincProductor;
        this.sincConsumidor = sincConsumidor;
        this.selectorAB = selectorAB;
        this.tipoConsumido = null;
        this.contadorAB = contadorAB;
    }

    @Override
    public void run() {
        int racha;
        System.out.println("TAREA(" + iD + ") ha comenzado su ejecución");

        try {
            comprobarElementos();
            racha = recuperarElementos();
            liberarProcesos(racha);

            System.out.println("TAREA(" + iD + ") ha finalizado su ejecución");
        } catch (InterruptedException ex) {
            System.out.println("TAREA(" + iD + ") CANCELADA");
        }
    }

    private void comprobarElementos() throws InterruptedException {
        System.out.println("TAREA(" + iD + ") espera consumir ");
        
        if (tipoElemento.equals(AoB)) {
            exmSelectorAB.acquire();
            contadorAB.añadir();
            exmSelectorAB.release();
        }
        
        sincConsumidor[tipoElemento.ordinal()].acquire();
        System.out.println("TAREA(" + iD + ") listo para consumir " + tipoElemento);
    }

    private int recuperarElementos() throws InterruptedException {
        Elemento elemento = null;
        int racha = 0;
        
        tipoConsumido = tipoElemento;
        if (tipoElemento.equals(AoB)) {
            exmSelectorAB.acquire();
            //El productor nos indica que tipo de elementos a reservado para los consumidores AB en este array
            tipoConsumido = selectorAB.remove(PRIMERO);
            exmSelectorAB.release();
        }
        
        exmBuffer.acquire();
        do {
            elemento = buffer.get(tipoConsumido);
            racha++;
        } while (!elemento.isFinRacha());
        exmBuffer.release();

        System.out.println("TAREA(" + iD + ") ha obtenido " + racha + " elementos de tipo "
                + tipoConsumido);
        TimeUnit.SECONDS.sleep(TIEMPO_CONSUMICION);

        return racha;
    }

    private void liberarProcesos(int racha) throws InterruptedException {
        boolean liberado;
        int tamRacha;

        // Comprobamos si se puede liberar algún productor porque hay
        // espacio en el Buffer, empezando con el que consuma más espacio del Buffer
        exmElm.acquire();
        elementos[tipoConsumido.ordinal()] -= racha;
        liberado = false;
        tamRacha = MAX_RACHA;
        while ((tamRacha >= MIN_RACHA) & !liberado) {
            if ((elementos[tipoConsumido.ordinal()] + tamRacha <= MAX_ELEMENTOS[tipoConsumido.ordinal()])
                    && sincProductor[tipoConsumido.ordinal()][tamRacha - 1].hasQueuedThreads()) {

                elementos[tipoConsumido.ordinal()] += tamRacha;
                liberado = true;
                sincProductor[tipoConsumido.ordinal()][tamRacha - 1].release();
            }

            tamRacha--;
        }
        exmElm.release();
    }
}
