/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.solucionpractica1;

import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.NO_ENCONTRADO;
import es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TipoElemento;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Buffer {
    private final ArrayList<Elemento> buffer;

    public Buffer() {
        this.buffer = new ArrayList();
    }
    
    /**
     * Añade un elemento al final del buffer
     * @param elemento
     * @return 
     */
    public boolean add(Elemento elemento) {
        return buffer.add(elemento);
    }
        
    /**
     * Devuelve y elimina el primer elemento del tipoElemento
     * @param tipoElemento
     * @return 
     */
    public Elemento get(TipoElemento tipoElemento) {
        int index;
        
        // Busca el índice del primer tipoElemento
        index = buffer.indexOf(new Elemento(tipoElemento));
        
        if( index == NO_ENCONTRADO )
            return null;
        
        return buffer.remove(index);
    }
    
    /**
     * Devuelve y elimina el primer elemento que coincida con su tipoElemento
     * @param elemento
     * @return 
     */
    public Elemento get(Elemento elemento) {
        int index;
        
        // Busca el índice del primer tipoElemento
        index = buffer.indexOf(elemento);
        
        if( index == NO_ENCONTRADO )
            return null;
        
        return buffer.remove(index);
    }

    @Override
    public String toString() {
        return "Buffer : " + buffer;
    }
}
