/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.solucionpractica1;

import es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TipoElemento;
import java.util.Objects;

/**
 *
 * @author pedroj
 */
public class Elemento {
    private final TipoElemento tipoElemento;
    private boolean finRacha;

    public Elemento(TipoElemento tipoElemento) {
        this.tipoElemento = tipoElemento;
        this.finRacha = false;
    }

    public Elemento(TipoElemento tipoElemento, boolean finRacha) {
        this.tipoElemento = tipoElemento;
        this.finRacha = finRacha;
    }
    
    public TipoElemento getTipoElemento() {
        return tipoElemento;
    }

    public boolean isFinRacha() {
        return finRacha;
    }

    public void setFinRacha(boolean finRacha) {
        this.finRacha = finRacha;
    }

    /**
     * Se comparan dos elementos segun su tipoElemento y si coincide son iguales
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Elemento other = (Elemento) obj;
        if (!Objects.equals(this.getTipoElemento(), other.getTipoElemento())) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Elemento{" + "tipoElemento=" + tipoElemento + ", finRacha=" + finRacha + '}';
    }
}
